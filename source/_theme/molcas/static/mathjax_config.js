//MathJax.Hub.Config({
window.MathJax = {
  "HTML-CSS": {
    scale: 90,
    preferredFont: "STIX",
    webFont: "STIX-Web",
  },
  TeX: {
    extensions: ["AMSmath.js","AMSsymbols.js","mhchem.js"],
    Macros: {
      mat: ["\\boldsymbol{\#1}", 1],
      sign: ["\\operatorname{sign}", 0],
      Tr: ["\\operatorname{Tr}", 0],
      abs: ["\\operatorname{abs}", 0],
      bra: ["\\left<\#1\\right|", 1],
      ket: ["\\left|\#1\\right>", 1],
      braket: ["\\left<\#1\\middle|\#2\\right>", 2],
      braopket: ["\\left<\#1\\middle|\#2\\middle|\#3\\right>", 3],
    },
  },
  SVG: {
    scale: 90,
    font: "STIX-Web",
  },
//});
};
